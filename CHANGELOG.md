Change Log
==========

3.4.0
-----

* Added method to allow for passing in a logging config file for C++ logging
* Updated ci such that py package allows failures
* Python 2 and Python 3 compatibility
* Remove trusty build, package and deploy CI jobs
* Run Python unit tests in CI
* Check if `/dev/log` exists before attempting to configure logger with syslog
* Added LICENSE.md

3.3.1
-----

* fixing source field in logging.conf

3.3.0
-----

* Added a Python logging .conf file and a method to find it
* Changed maintainer
