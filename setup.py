# @file setup.py
# @copyright Copyright 2017 NASA. All Rights Reserved.

from __future__ import print_function
from catkin_pkg.python_setup import generate_distutils_setup
import os
import sys

if 'ROS_DISTRO' in os.environ:
    from distutils.core import setup
else:
    from setuptools import setup

d = generate_distutils_setup(
    packages=['nasa_common_logging'],
    package_dir={'': 'src'},
    package_data={'nasa_common_logging': ['python_logging.conf']}
)

try:
    from nasa_common_pkg.pep440 import update_setup
    d = update_setup(d)
except ImportError:
    print('Unable to find nasa_common_pkg, package will not include repo details', file=sys.stderr)

setup(**d)
