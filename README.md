# nasa\_common\_logging

## About

See [package.xml](./package.xml) for version and maintainer information.

See [LICENSE.md](./LICENSE.md) for complete license information.  

This C++ library and Python package provide a standard way to instantiate, configure, and use logging.  The C++ system provides a singleton Logger class as a front-end for log4cpp.  The Python system provides methods that help load a common or custom configuration into the standard `logging` infrastructure.

## C++ Example

```c++
    #include "nasa_common_logging/Logger.h"

    int main(int argc, char** argv)
    {
        // use log() for simple messages
        // requires a category that describes the source of the log message, then a severity level, then a standard std::string message.
        NasaCommonLogging::Logger::log("gov.nasa.nasa_common_logging.logdemo", log4cpp::Priority::WARN, "This is a warning!");

        // use getCategory() for streamed messages
        // requires a category that describes the source of the log messages, then a severity level, then any number of stream objects
        NasaCommonLogging::Logger::getCategory("gov.nasa.nasa_common_logging.logdemo") << log4cpp::Priority::ERROR << "This is error: " << 42;

        return 0;
    }
```

## Python Examples

Using nasa_common_logging defaults:

```python
    import logging
    import nasa_common_logging    

    nasa_common_logging.logging_utils.configure_common_logging()

    logger = logging.getLogger(__name__)

    logger.info('hello world')
    logger.log(nasa_common_logging.log_level['error'], 'hey world!')

    sublog = logger.getChild('sub')
    sublog.log(nasa_common_logging.log_level['CRITICAL'], 'for real, world, where you at?!')

    my_custom_logging_config = nasa_common_logging.logging_utils.get_common_logging_configuration()
    my_custom_logging_config['handlers']['console']['formatter'] = 'brief'
    nasa_common_logging.logging_utils.configure_custom_logging(my_custom_logging_config)
    logger.log(nasa_common_logging.log_level['warn'], 'custom log format')
```

Using nasa_common_logging loading a custom config file:

```python
    #!/usr/bin/env python
    import logging
    import nasa_common_logging

    import os
    import rospkg
    ncl_path = rospkg.RosPack().get_path('nasa_common_logging')
    config_file = os.path.join(ncl_path, 'share', 'logging.json')
    nasa_common_logging.logging_utils.configure_custom_logging_file(config_file)
```

Using nasa_common_logging, overriding the defaults:

```python
    import logging
    import nasa_common_logging

    # Set the log level:
    nasa_common_logging.configure_common_logging(level='WARNING')

    # Override the handlers to only use a syslog and file handler
    # All options are 'syslog', 'file', 'socket', and 'console'
    nasa_common_logging.configure_common_logging(handlers=['console', 'file'])

    # Override the file path.  This will log to `~/.log/myApp.log`
    nasa_common_logging.configure_common_logging(file_path='myApp.log')

    # This will log to `/usr/local/log/myApp.log`
    nasa_common_logging.configure_common_logging(file_path='/usr/local/log/myApp.log')

    logger = logging.getLogger(__name__)
    logger.info('hello world!')
```

One could also grab the default config, modify it, and set it.  The config is a python dictionary.

```python
    import logging
    import nasa_common_logging

    config = nasa_common_logging.get_common_logging_configuration()
    config['root']['level'] = 'WARNING'
    config['root']['handlers'].append('file')
    nasa_common_logging.configure_custom_logging(config)
```
