//! @file Logger.cpp
#include <memory>
#include "nasa_common_logging/Logger.h"


using namespace log4cpp;

namespace NasaCommonLogging
{
    Logger::Logger()
        : propertyFile("log4cpp.properties")
        , packagePath(ros::package::getPath("nasa_common_logging"))
    {
        std::string localPropertyFile = "./" + propertyFile;

        try
        {
            PropertyConfigurator::configure(localPropertyFile);
        }
        catch (ConfigureFailure& e)
        {
            if (not (std::string(e.what()) == "File " + localPropertyFile + " does not exist"))
            {
                std::cerr << "WARN: Could not load local dir property file [" << localPropertyFile <<  "]: " << e.what() << std::endl;
            }

            std::string packagePropertyFile = packagePath + "/share/" + propertyFile;

            try
            {
                PropertyConfigurator::configure(packagePropertyFile);
            }
            catch (ConfigureFailure& e)
            {
                std::cerr << "WARN: Could not load package dir property file [" << packagePropertyFile <<  "]: " << e.what() << std::endl;

                Appender* rootAppender = new OstreamAppender("root", &std::cout);
                Layout* rootLayout = new BasicLayout();
                rootAppender->setLayout(rootLayout);
                Category::getRoot().addAppender(rootAppender);
                Category::getRoot().setPriority(Priority::WARN);

                Category::getRoot().log(Priority::WARN, "Could not load local dir or package dir property file: " + std::string(e.what()) + ". Using defaults of [std::out] and [WARN].");
            }
        }
    }

    void Logger::configureCustomLoggingFile(const std::string& configurationFile)
    {
        try
        {
            PropertyConfigurator::configure(configurationFile);
        }
        catch (ConfigureFailure& e)
        {
            std::cerr << "WARN: Could not load logging configuration file [" << configurationFile <<  "]: " << e.what() << std::endl;
            std::shared_ptr<Appender> rootAppender(new OstreamAppender("root", &std::cout));
            std::shared_ptr<Layout> rootLayout(new BasicLayout());
            rootAppender->setLayout(rootLayout.get());
            Category::getRoot().addAppender(rootAppender.get());
            Category::getRoot().setPriority(Priority::WARN);

            Category::getRoot().log(Priority::WARN, "Could not load local dir or package dir property file: " + std::string(e.what()) + ". Using defaults of [std::out] and [WARN].");
        }
    }

    Logger::~Logger()
    {
    }

    void Logger::log(const std::string& category, log4cpp::Priority::Value priority, const std::string& message)
    {
        getCategory(category).log(priority, message);
    }

    log4cpp::Category& Logger::getCategory(const std::string& category)
    {
        return Category::getInstance(category);
    }

    static Logger singleton;
}
