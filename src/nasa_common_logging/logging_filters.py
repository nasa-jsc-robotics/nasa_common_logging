import logging


class IgnoreIfContainsFilter(logging.Filter):
    """
    Filter out messages that contain a certain string.

    If a message contains any of the substrings in 'params', the message
    will get filtered.

    To add to the config as a filter called "ignore_status",
    "filters" : {
        "ignore_status": {
            "()" : "nasa_common_logging.IgnoreIfContainsFilter",
            "params" : ["name:status","state:\"COMPLETE\"", "args=(), kwargs={}", "callback"]
        }
    }
    """

    def __init__(self, params=[]):
        """
        @type params list
        @param params List of strings to search for filtering
        """
        self.params = params

    def filter(self, record):
        allow = True
        for param in self.params:
            if str(param) in str(record.msg):
                allow = False

        return allow
