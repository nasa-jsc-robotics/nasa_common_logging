import unittest

from nasa_common_logging.logging_utils import parse_log_buffer


class TestParseSyslogLine(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_parse_common_logging_basic(self):
        timestamp = "2016-01-01 00:00:00"
        hostname  = "hostname"
        source_name = "source_name"
        level = "INFO"
        category = "category.test"
        message = "test message!"

        buffer = "{} {} {}: [{}] [{}] {}".format(timestamp, hostname, source_name, level, category, message)
        t, host, src, lvl, cat, msg = parse_log_buffer(buffer)[0]

        self.assertEqual(timestamp, t)
        self.assertEqual(hostname, host)
        self.assertEqual(source_name, src)
        self.assertEqual(level, lvl)
        self.assertEqual(category, cat)
        self.assertEqual(message, msg)

    def test_parse_common_logging_space_in_names(self):
        timestamp = "2016-09-01 15:53:23"
        hostname = "hydrophobe"
        source_name = "ncl_py"
        level = "INFO"
        category = "root"
        message = "test"

        buffer = "{} {} {}: [{}   ] [{}      ] {}\n".format(timestamp, hostname, source_name, level, category, message)
        t, host, src, lvl, cat, msg = parse_log_buffer(buffer)[0]

        self.assertEqual(timestamp, t)
        self.assertEqual(hostname, host)
        self.assertEqual(source_name, src)
        self.assertEqual(level, lvl)
        self.assertEqual(category, cat)
        self.assertEqual(message, msg)

    #@unittest.skip("reasons")
    def test_parse_common_logging_multiline(self):
        timestamp = "2016-01-01 00:00:00"
        hostname  = "hostname"
        source_name = "source_name"
        level = "INFO"
        category = "category.test"
        message = "This is a long test message.\nIt has two lines."

        buffer = "{} {} {}: [{}] [{}] {}".format(timestamp, hostname, source_name, level, category, message)
        t, host, src, lvl, cat, msg = parse_log_buffer(buffer)[0]

        self.assertEqual(timestamp, t)
        self.assertEqual(hostname, host)
        self.assertEqual(source_name, src)
        self.assertEqual(level, lvl)
        self.assertEqual(category, cat)
        self.assertEqual(message, msg)

    #@unittest.skip("reasons")
    def test_parse_common_logging_two_messages(self):
        timestamp1 = "2016-01-01 00:00:00"
        hostname1 = "hostname"
        source_name1 = "source_name"
        level1 = "INFO"
        category1 = "category.test"
        message1 = "test message!"
        line1 = "{} {} {}: [{}] [{}] {}".format(timestamp1, hostname1, source_name1, level1, category1, message1)

        timestamp2 = "2016-02-02 00:00:02"
        hostname2 = "hostname2"
        source_name2 = "source_name2"
        level2 = "INFO2"
        category2 = "category.test2"
        message2 = "test message2!"
        line2 = "{} {} {}: [{}] [{}] {}".format(timestamp2, hostname2, source_name2, level2, category2, message2)

        buffer = line1 + '\n' + line2

        log_entries = parse_log_buffer(buffer)

        log_entry1 = log_entries[0]
        self.assertEqual(timestamp1,log_entry1[0])
        self.assertEqual(hostname1,log_entry1[1])
        self.assertEqual(source_name1,log_entry1[2])
        self.assertEqual(level1,log_entry1[3])
        self.assertEqual(category1,log_entry1[4])
        self.assertEqual(message1,log_entry1[5])

        log_entry2 = log_entries[1]
        self.assertEqual(timestamp2,log_entry2[0])
        self.assertEqual(hostname2,log_entry2[1])
        self.assertEqual(source_name2,log_entry2[2])
        self.assertEqual(level2,log_entry2[3])
        self.assertEqual(category2,log_entry2[4])
        self.assertEqual(message2,log_entry2[5])

    #@unittest.skip("reasons")
    def test_parse_common_logging_multiline_messages(self):
        timestamp1 = "2016-01-01 00:00:00"
        hostname1 = "hostname"
        source_name1 = "source_name"
        level1 = "INFO"
        category1 = "category.test"
        message1 = "test message line1\nAnd line2\nand line3"
        line1 = "{} {} {}: [{}] [{}] {}".format(timestamp1, hostname1, source_name1, level1, category1, message1)

        timestamp2 = "2016-02-02 00:00:02"
        hostname2 = "hostname2"
        source_name2 = "source_name2"
        level2 = "INFO2"
        category2 = "category.test2"
        message2 = "test message2"
        line2 = "{} {} {}: [{}] [{}] {}".format(timestamp2, hostname2, source_name2, level2, category2, message2)

        buffer = line1 + '\n' + line2

        log_entries = parse_log_buffer(buffer)

        log_entry1 = log_entries[0]
        self.assertEqual(timestamp1, log_entry1[0])
        self.assertEqual(hostname1, log_entry1[1])
        self.assertEqual(source_name1, log_entry1[2])
        self.assertEqual(level1, log_entry1[3])
        self.assertEqual(category1, log_entry1[4])
        self.assertEqual(message1, log_entry1[5])

        log_entry2 = log_entries[1]
        self.assertEqual(timestamp2, log_entry2[0])
        self.assertEqual(hostname2, log_entry2[1])
        self.assertEqual(source_name2, log_entry2[2])
        self.assertEqual(level2, log_entry2[3])
        self.assertEqual(category2, log_entry2[4])
        self.assertEqual(message2, log_entry2[5])

    #@unittest.skip("reasons")
    def test_parse_syslog_basic(self):
        timestamp = "Jan 23 00:00:00"
        hostname = "hostname"
        source_name = "source_name"
        level = "INFO"
        category = "category.test"
        message = "test message!"

        buffer = "{} {} {}: [{}] [{}] {}".format(timestamp, hostname, source_name, level, category, message)
        t, host, src, lvl, cat, msg = parse_log_buffer(buffer)[0]

        self.assertEqual(timestamp, t)
        self.assertEqual(hostname, host)
        self.assertEqual(source_name, src)
        self.assertEqual(level, lvl)
        self.assertEqual(category, cat)
        self.assertEqual(message, msg)

    # @unittest.skip("reasons")
    def test_parse_syslog_basic_single_digit_day(self):
        timestamp = "Jan  1 00:00:00"
        hostname = "hostname"
        source_name = "source_name"
        level = "INFO"
        category = "category.test"
        message = "test message!"

        buffer = "{} {} {}: [{}] [{}] {}".format(timestamp, hostname, source_name, level, category, message)
        t, host, src, lvl, cat, msg = parse_log_buffer(buffer)[0]

        self.assertEqual(timestamp, t)
        self.assertEqual(hostname, host)
        self.assertEqual(source_name, src)
        self.assertEqual(level, lvl)
        self.assertEqual(category, cat)
        self.assertEqual(message, msg)

        # @unittest.skip("reasons")

    def test_parse_syslog_basic_single_warn(self):
        timestamp = "Jan  1 00:00:00"
        hostname = "hostname"
        source_name = "source_name"
        level = "WARN"
        category = "category.test"
        message = "test message!"

        buffer = "{} {} {}: [{}] [{}] {}".format(timestamp, hostname, source_name, level, category, message)
        t, host, src, lvl, cat, msg = parse_log_buffer(buffer)[0]

        self.assertEqual(timestamp, t)
        self.assertEqual(hostname, host)
        self.assertEqual(source_name, src)
        self.assertEqual(level, lvl)
        self.assertEqual(category, cat)
        self.assertEqual(message, msg)

    #@unittest.skip("reasons")
    def test_mix_log_types(self):
        timestamp1 = "Jan 01 01:01:01"
        hostname1 = "hostname"
        source_name1 = "source_name"
        level1 = "INFO"
        category1 = "category.test"
        message1 = "test message!"
        line1 = "{} {} {}: [{}] [{}] {}".format(timestamp1, hostname1, source_name1, level1, category1, message1)

        timestamp2 = "2016-02-02 00:00:02"
        hostname2 = "hostname2"
        source_name2 = "source_name2"
        level2 = "INFO2"
        category2 = "category.test2"
        message2 = "test message2!"
        line2 = "{} {} {}: [{}] [{}] {}".format(timestamp2, hostname2, source_name2, level2, category2, message2)

        buffer = line1 + '\n' + line2

        log_entries = parse_log_buffer(buffer)

        log_entry1 = log_entries[0]
        self.assertEqual(timestamp1, log_entry1[0])
        self.assertEqual(hostname1, log_entry1[1])
        self.assertEqual(source_name1, log_entry1[2])
        self.assertEqual(level1, log_entry1[3])
        self.assertEqual(category1, log_entry1[4])
        self.assertEqual(message1, log_entry1[5])

        log_entry2 = log_entries[1]
        self.assertEqual(timestamp2, log_entry2[0])
        self.assertEqual(hostname2, log_entry2[1])
        self.assertEqual(source_name2, log_entry2[2])
        self.assertEqual(level2, log_entry2[3])
        self.assertEqual(category2, log_entry2[4])
        self.assertEqual(message2, log_entry2[5])

    def test_parse_generic_logging_basic(self):
        timestamp = "2016-01-01 00:00:00"
        hostname = "hostname"
        source_name = "source_name"
        message = "test message!"

        buffer = "{} {} {}: {}".format(timestamp, hostname, source_name, message)
        t, host, src, lvl, cat, msg = parse_log_buffer(buffer)[0]

        self.assertEqual(timestamp, t)
        self.assertEqual(hostname, host)
        self.assertEqual(source_name, src)
        self.assertEqual(None, lvl)
        self.assertEqual(None, cat)
        self.assertEqual(message, msg)

    def test_parse_generic_logging_multi_line(self):
        timestamp = "2016-01-01 00:00:00"
        hostname = "hostname"
        source_name = "source_name"
        message = "test message!\nTwo lines long"

        buffer = "{} {} {}: {}".format(timestamp, hostname, source_name, message)
        t, host, src, lvl, cat, msg = parse_log_buffer(buffer)[0]

        self.assertEqual(timestamp, t)
        self.assertEqual(hostname, host)
        self.assertEqual(source_name, src)
        self.assertEqual(None, lvl)
        self.assertEqual(None, cat)
        self.assertEqual(message, msg)


if __name__ == "__main__":
    unittest.main()
