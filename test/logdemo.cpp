#include "nasa_common_logging/Logger.h"

int main(int argc, char** argv)
{
    // NasaCommonLogging::Logger examples
    // keep log messages to a single line (no \n), otherwise logfiles get wonky. don't end a message with a newline either.

    // use log() for simple messages
    // requires a category that describes the source of the log message, then a severity level, then a standard std::string message.
    NasaCommonLogging::Logger::log("gov.nasa.nasa_common_logging.logdemo", log4cpp::Priority::WARN, "This is a warning!");

    // use getCategory() for streamed messages
    // requires a category that describes the source of the log messages, then a severity level, then any number of stream objects
    NasaCommonLogging::Logger::getCategory("gov.nasa.nasa_common_logging.logdemo") << log4cpp::Priority::ERROR << "This is error: " << 42;

    return 0;
}
