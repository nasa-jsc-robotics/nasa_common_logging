#include "nasa_common_logging/Logger.h"

int main(int argc, char** argv)
{
    NasaCommonLogging::Logger::log("gov.nasa.nasa_common_logging.logdemo", log4cpp::Priority::WARN, "This is a warning!");
    NasaCommonLogging::Logger::configureCustomLoggingFile("./test_logging.properties");
    NasaCommonLogging::Logger::log("gov.nasa.nasa_common_logging.logdemo", log4cpp::Priority::WARN, "This is a warning!");

    return 0;
}