#!/usr/bin/env python
import unittest
import logging
import nasa_common_logging

nasa_common_logging.logging_utils.configure_common_logging(handlers=["console"])


class TestParseSyslogLine(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_log(self):
        logger = logging.getLogger(__name__)

        logger.info('hello world')
        logger.log(nasa_common_logging.log_level['error'], 'hey world!')

        try:
            a = 1. / 0.
        except ZeroDivisionError as e:
            logger.error('Division by 0')

        sublog = logger.getChild('sub')
        sublog.log(nasa_common_logging.log_level['CRITICAL'], 'for real, world, where you at?!')


if __name__ == "__main__":
    unittest.main()
